import passport from 'passport';
import localStrategy from 'passport-local';
import JwtStrategy from 'passport-jwt';

import UserModel from '../models/userModel';


passport.use('signup', new localStrategy.Strategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, async (request, email, password, done) => {
    try {
        const { username } = request.body;
        const user = await UserModel.create({ email, password, username });
        return done(null, user);
    } catch (error) {
        return done(error);
    }


}));

passport.use('login', new localStrategy.Strategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true

}, async (request, email, password, done) => {
    try {
        const user = await UserModel.findOne({ email });
        if (!user) {
            return done(new Error('User not found'), false);
        }
        const valid = await user.isValidPassword(password);
        if (!valid) {
            return done(new Error('Invalid password'), false);
        }
        console.log(user)
        return done(null, user);
    } catch (error) {
        return done(error);
    }
}));


passport.use(new JwtStrategy.Strategy({
    secretOrKey: process.env.JWT_SECRET,
    jwtFromRequest: (request) => {
        let token = null;
        if (request && request.cookies) token = request.cookies.jwt;
        return token;
    },
}, async (token, done) => {
    try {
        return done(null, token.user);
    } catch (error) {
        return done(error);
    }
}));