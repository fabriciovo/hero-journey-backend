import express from 'express';
import hbs from 'nodemailer-express-handlebars';
import nodemailer from 'nodemailer';
import path from 'path';
import crypto from 'crypto';

import UserModel from '../models/userModel';


const router = express.Router();
const email = process.env.EMAIL;
const password = process.env.PASSWORD;

const smtpTransport = nodemailer.createTransport({
    service: process.env.EMAIL_PROVIDER,
    auth: {
        user: email,
        pass: password
    }
})

const handlebarsOptions = {
    viewEngine: {
        extName: '.hbs',
        partialsDir: './templates/',
        layoutsDir: './templates/',
        defaultLayout: ''
    },
    viewPath: path.resolve('./templates/'),
    extName: '.html'
}

smtpTransport.use('compile', hbs(handlebarsOptions));

router.post('/forgot-password', async (request, response) => {
    const userEmail = request.body.email;
    const user = await UserModel.findOne({ email: userEmail });

    if (!user) {
        response.status(400).json({
            message: 'invalid email',
            status: 400
        });
        return;
    }

    const buffer = crypto.randomBytes(20);
    const token = buffer.toString('hex');

    if (!request.body || !request.body.email) {
        response.status(400).json({
            message: 'invalid body',
            status: 400
        });
    } else {

        const emailOptions = {
            to: userEmail,
            from: email,
            template: 'forgot-password',
            subject: 'Teste',
            defaultLayout: null,
            context: {
                url: `http://localhost:${process.env.PORT || 3000}/reset-password.html?token=${token}`
            }
        }
        console.log(emailOptions)
        await smtpTransport.sendMail(emailOptions, (error, info) => {
            console.log(error);
            console.log(info);
        });

        response.status(200).json({
            message: 'An email has been sent to your email address',
            status: 200
        });

        await UserModel.findByIdAndUpdate({ _id: user._id }, { resetToken: token, resetTokenExp: Date.now() + 60000 });
    }




});

router.post('/reset-password', async (request, response) => {
    const userEmail = request.body.email;
    const user = await UserModel.findOne({ resetToken: request.body.token, resetTokenExp: { $gt: Date.now() }, email: userEmail });

    if (!user) {
        response.status(400).json({
            message: 'invalid token',
            status: 400
        });
        return;
    }

    if(!request.body.password || !request.body.verifiedPassword ||  request.body.password !== request.body.verifiedPassword){
        response.status(400).json({
            message: 'passwords do not match',
            status: 400
        });
        return;
    }

    user.password = request.body.password;
    user.resetToken = undefined;
    user.resetTokenExp = undefined;

    await user.save();

    const emailOptions = {
        to: userEmail,
        from: email,
        template: 'reset-password',
        subject: 'Teste',
        defaultLayout: null,
        context: {
            name:user.username
        }
    }
    await smtpTransport.sendMail(emailOptions);
    response.status(200).json({
        message: 'Password Updated',
        status: 200
    });


});

export default router;