import express from 'express';
import passport from 'passport';
import jwt from 'jsonwebtoken';
import { request, response } from 'express';


const router = express.Router();
const tokenList = { };

function processLogoutRequest(){
    if (request.cookies) {
        const refresToken = request.cookies.refresJwt;
        if (refresToken in tokenList) delete tokenList[refresToken];
        response.clearCookie('jwt');
        response.clearCookie('refreshJwt');
    }

    if(request.method === 'POST'){
        response.status(200).json({
            message: 'Logged out',
            status: 200
        });
    }else if(request.method === 'GET') {
        response.sendFile('logout.html', {root: './public'});
        response.status(200).json({
            message: 'Logged out get',
            status: 200
        });
    }

router.get('/', (request, response) => {
    response.send('Hello World');
})

router.get('/status', passport.authenticate('jwt', { session: false }), async (request, response) => {
    response.status(200).json({
        message: 'ok',
        status: 200
    });
})


router.post('/signup', passport.authenticate('signup', { session: false }), async (request, response) => {
    response.status(200).json({
        message: 'ok',
        status: 200
    });
});

router.post('/login', async (request, response, next) => {
    passport.authenticate('login', async (err, user) => {
        try {
            if (err) {
                return next(err);
            }

            if (!user) {
                return next(new Error("Email and Password are required"));
            }

            request.login(user, { session: false }, (err) => {
                if (err) return next(err);


                const body = {
                    _id: user._id,
                    email: user.email,
                    name: user.username
                }
                const token = jwt.sign({ user: body }, process.env.JWT_SECRET, { expiresIn: 300 });
                const refresToken = jwt.sign({ user: body }, process.env.JWT_REFRESH_SECRET, { expiresIn: 86400 });


                response.cookie('jwt', token);
                response.cookie('refreshJwt', refresToken);

                tokenList[refresToken] = {
                    token,
                    refresToken,
                    email: user.email,
                    _id: user._id,
                    name: user.name
                }

                return response.status(200).json({ token, refresToken, status: 200 });
            });
        } catch {
            console.log(err)
            return next(err)
        }
    })(request, response, next);
});


router.route('/logout').get(processLogoutRequest).post(processLogoutRequest);


router.post('/token', (request, response) => {
    const { refresToken } = request.body;
    if (refresToken in tokenList) {
        const body = {
            email:  tokenList[refresToken].email,
            _id: tokenList[refresToken]._id,
            name: tokenList[refresToken].name
        };
        const token = jwt.sign({user:body}, process.env.JWT_SECRET, {expiresIn:300});

        response.cookie('jwt', token);
        tokenList[refresToken].token = token;

        response.status(200).json({
            token,
            status: 200
        })
    } else {
        response.status(401).json({
            message: 'unauthorized',
            status: 401
        })
    }
});




}

export default router;