import mongoose  from 'mongoose';
import bcrypt  from 'bcrypt';

const {Schema} = mongoose;

const UserSchema = new Schema ({
    email:{
        type:String,
        require:true,
        unique:true
    },
    password:{
        type:String,
        require:true,
    },
    username:{
        type:String,
        require:true,
    },
    resetToken:{
        type:String,
    },
    resetTokenExp:{
        type:Date,
    }
});

UserSchema.pre('save', async function(next){
    const hash = await bcrypt.hash(this.password,10);
    this.password = hash;
    next();
});

UserSchema.methods.isValidPassword = async function(password){
    const user = this;
    const compare = await bcrypt.compare(password,user.password);
    return compare;
}


const UserModel = mongoose.model('user',UserSchema);

export default UserModel;