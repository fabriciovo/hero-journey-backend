
function postData(url = '', data = { }) {
    return fetch(url, {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'include',
        headers: { 'Content-Type': 'application/json' },
        redirect: 'follow',
        referrer: 'no-referer',
        body: JSON.stringify(data)
    }).then(response => {
        debugger
        return response.json()
    });
}

function signIn() {
    const body = {
        email: document.forms[0].elements[0].value,
        password: document.forms[0].elements[1].value,
    }
    postData('/login', body).then(response => {
        if (response.status !== 200) {
            throw new Error(response.error);
        }
        window.location.replace('/game.html');
    }).catch(error => {
        window.alert(error.message);
    })


}


function signup() {
    const body = {
        email: document.forms[0].elements[0].value,
        password: document.forms[0].elements[1].value,
        username: document.forms[0].elements[2].value,
    }

    postData('/signup', body).then(response => {
        if (response.status !== 200) {
            throw new Error(response.error);
        }
        window.alert('User created');
        window.location.replace('/index.html');
    }).catch(error => {
        window.alert(error.message);
    })


}

function resetPassword() {
    const password = document.forms[0].elements[1].value
    const verifiedPassword = document.forms[0].elements[2].value
    const body = {
        email: document.forms[0].elements[0].value,
        password: password,
        verifiedPassword: verifiedPassword, 
        token:document.location.href.split('token=')[1]
    }

    console.log(body);
    if(password !== verifiedPassword){
        window.alert('Password do not match');
    }else{
        postData('/reset-password', body).then(response => {
            if (response.status !== 200) {
                throw new Error(response.error);
            }
            window.alert('Password Updated');
            window.location.replace('/index.html');
        }).catch(error => {
            window.alert(error.message);
        })
    }


}

function forgotPassword() {
    const body = {
        email: document.forms[0].elements[0].value,
    }

    postData('/forgot-password', body).then(response => {
        if (response.status !== 200) {
            throw new Error(response.error);
        }
        window.alert('Password Email Sent');
        window.location.replace('/index.html');
    }).catch(error => {
        window.alert(error.message);
    })
}

